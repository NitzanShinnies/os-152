#include "jobctrl.h"
#include "types.h"
#include "defs.h"
#include "param.h"
#include "spinlock.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"

struct {
	struct spinlock lock;
	struct job job[NJOBS];
} jtable;


int nextjid=1;

void jinit(void) {
	initlock(&jtable.lock, "jtable");
}

void alloc_job(char* jobname) {
	struct job *j;
  	acquire(&jtable.lock);
  	for(j = jtable.job; j < &jtable.job[NJOBS]; j++)
    	if(j->state == JOB_IDLE)
      	goto found;
  	release(&jtable.lock);
  	return;

  	found:
  	j->jid=nextjid++;
	j->GUID=proc->pid;
	j->state=JOB_RUNNING;

    strncpy(j->name,jobname,strlen(jobname)+1);
  	currentJob=j;
    release(&jtable.lock);
} 

void dealloc_job(int GUID) {
	cprintf("deallocating job for GUID %d!\n",GUID);
	struct job *j;
	acquire(&jtable.lock);

	for (j=jtable.job;j<&jtable.job[NJOBS];j++) {
		if (j->GUID == GUID) {
			goto found;
		}
	}

	release(&jtable.lock);
	return;

	found:

	cprintf("dallocing for job %d\n",j->jid);

	j->GUID=0;
	j->jid=0;
	j->state=JOB_IDLE;
	(*j->name)=0;

	release(&jtable.lock);

}

void echo_jobs(void) {
	struct job *j;
	int _printed=0;

	acquire(&jtable.lock);
	for(j = jtable.job; j < &jtable.job[NJOBS]; j++) {
		if (j->state == JOB_RUNNING) {
			_printed=1;
			cprintf("Job %d: %s\n",j->jid,j->name);
			echo_procs(j->GUID);
		}
	}

	release(&jtable.lock);

	if (!_printed) {
		cprintf("There are no jobs\n");
	}

}

int get_guid(int jid) {
	struct job *j;
	acquire(&jtable.lock);

	for (j = jtable.job; j < &jtable.job[NJOBS]; j++) {
		if (j->jid == jid) {
			goto found;
		}
	}

	release(&jtable.lock);
	return -1;

	found:
	release(&jtable.lock);

	return j->GUID;
}

int get_first_active_job(void) {
	struct job *j;
	acquire(&jtable.lock);

	for (j = jtable.job; j < &jtable.job[NJOBS]; j++) {
		if (j->state == JOB_RUNNING) {
			goto found;
		}
	}

	release(&jtable.lock);
	return -1;

	found:
	release(&jtable.lock);

	return j->jid;
}
