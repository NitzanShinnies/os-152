#define NJOBS 64
#define JOB_NAME_SZ 256

enum jobstate { JOB_IDLE, JOB_RUNNING };

struct job {
	char name[JOB_NAME_SZ]; 	// job name
	enum jobstate state;		// job state
	int jid;					// job id
	int GUID;					// GUID
};

