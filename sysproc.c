#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "jobctrl.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void) // TODO: check if it should be int instead of void.
{
  exit(0);
  return 0;  // not reached
}

int
sys_wait(int *status)// TODO: check how we pass the int ptr to wait
{
  return wait(0);
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;
  
  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_jobs(void) {
  echo_jobs();
  return 0;
}

int sys_allocjob(void) {
  char *name;
  argstr(0,&name);
  alloc_job(name);
  return 0;
}

int sys_fg(void) {
  int jid,pid;

  if (!argint(0,&jid)) {
    jid=get_first_active_job();
  }

  pid=get_last_live_pid(get_guid(jid));
  yield_to(pid);

  return 0;
}